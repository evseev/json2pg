### Testing environment:

* Ubuntu 14.04 LTS

### Required packages:

* socat _(for testing only)_
* postgresql
* libanyevent-dbd-pg-perl
* libconfig-tiny-perl

### Run:

* json2pg /path/to/configuration_file

### Configuration file:

* verbose = 0
* listen_ipaddr = 127.0.0.1
* listen_port = 5678
* dbname = db1
* dbuser = <empty>
* dbpass = <empty>

### Database init:

* sudo -Hiu postgres
* createuser -sw "$SUDO_USER"
* createdb -O "$SUDO_USER" db1
* psql db1
* create table table1(id serial, name varchar(80), value int);
* insert into table1(name, value) values ('Good', 100), ('Bad', 200), ('Ugly', 300);

### Test:

* In the first console: `./json2pg`
* In the second console: `{ ./hash2json table1 name; ./hash2json table1 value; sleep 1; } | socat - TCP:127.0.0.1:5678`
